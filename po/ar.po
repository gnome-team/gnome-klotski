# translation of gnome-games.HEAD.po to Arabic
# Sayed Jaffer Al-Mosawi <mosawi@arabeyes.org>, 2002.
# Arafat Medini <lumina@silverpen.de>, 2003.
# Abdulaziz Al-Arfaj <alarfaj0@yahoo.com>, 2004.
# Djihed Afifi <djihed@gmail.com>, 2006.
# Meno25 , 2007.
# Khaled Hosny <khaledhosny@eglug.org>, 2007, 2008, 2009, 2011.
# Ahmad Farghal <ahmad.farghal@gmail.com>, 2007.
# Anas Husseini <linux.anas@gmail.com>, 2007.
# Abdelmonam Kouka <abdelmonam.kouka@ubuntume.com>, 2008.
# Anas Afif Emad <anas.e87@gmail.com>, 2008.
# Ibrahim Saed <ibraheem5000@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: gnome-games.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-01 23:36+0100\n"
"PO-Revision-Date: 2012-08-20 05:27+0200\n"
"Last-Translator: Ibrahim Saed <ibraheem5000@gmail.com>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Virtaal 0.7.1\n"
"X-Project-Style: gnome\n"

#: ../data/gnome-klotski.appdata.xml.in.h:1
#, fuzzy
msgid "GNOME Klotski"
msgstr "كلوتسكي"

#: ../data/gnome-klotski.appdata.xml.in.h:2
#: ../data/gnome-klotski.desktop.in.h:2
msgid "Slide blocks to solve the puzzle"
msgstr "قم بإزاحة المكعبات لتحل الأحجية"

#: ../data/gnome-klotski.appdata.xml.in.h:3
msgid ""
"GNOME Klotski is a set of block sliding puzzles. The objective is to move "
"the patterned block to the area bordered by green markers. To do so, you’ll "
"need to slide other blocks out of the way. Complete each puzzle in as few "
"moves as possible!"
msgstr ""

#: ../data/gnome-klotski.appdata.xml.in.h:4
msgid ""
"GNOME Klotski comes with nearly thirty different puzzle layouts of varying "
"difficulty. Some of the layouts are similar and only the size of one or more "
"blocks differ. Other layouts have more variety."
msgstr ""

#: ../data/gnome-klotski.appdata.xml.in.h:5
msgid "The GNOME Project"
msgstr ""

#: ../data/gnome-klotski.desktop.in.h:1 ../data/klotski.ui.h:6
#: ../src/gnome-klotski.vala:66 ../src/gnome-klotski.vala:112
msgid "Klotski"
msgstr "كلوتسكي"

#: ../data/gnome-klotski.desktop.in.h:3
msgid "game;strategy;logic;move;"
msgstr ""

#: ../data/klotski.ui.h:1
#, fuzzy
msgid "Huarong Trail"
msgstr "هوارونغ"

#: ../data/klotski.ui.h:2
msgid "Challenge Pack"
msgstr "حزمة التحدي"

#: ../data/klotski.ui.h:3
msgid "Skill Pack"
msgstr "حزمة المهارات"

#: ../data/klotski.ui.h:4
#, fuzzy
msgid "Previous"
msgstr "اللغز السابق"

#: ../data/klotski.ui.h:5
#, fuzzy
msgid "Next"
msgstr "التّالي:"

#: ../data/klotski.ui.h:7
#, fuzzy
msgid "_Start Over"
msgstr "_ابدأ"

#: ../data/klotski.ui.h:8
#, fuzzy
msgid "Restart the current puzzle"
msgstr "أعد اللعبة الحالية"

#: ../data/klotski.ui.h:9
#, fuzzy
msgid "_Change Puzzle"
msgstr "لغز بنت"

#: ../data/klotski.ui.h:10
#, fuzzy
msgid "Choose an other puzzle"
msgstr "اختر مكان النتيجة."

#: ../data/klotski-menus.ui.h:1
msgid "_Scores"
msgstr "ال_نتائج"

#: ../data/klotski-menus.ui.h:2
msgid "_Help"
msgstr "_مساعدة"

#: ../data/klotski-menus.ui.h:3
msgid "_About"
msgstr "_عَنْ"

#: ../data/klotski-menus.ui.h:4
msgid "_Quit"
msgstr "ا_خرُج"

#: ../data/org.gnome.klotski.gschema.xml.h:1
msgid "The puzzle in play"
msgstr "الأحجية الملعوبة"

#: ../data/org.gnome.klotski.gschema.xml.h:2
msgid "The number of the puzzle being played."
msgstr "رقم الأحجية الملعوبة."

#: ../data/org.gnome.klotski.gschema.xml.h:3
#, fuzzy
msgid "Width of the window in pixels"
msgstr "عرض النافذة الرئيسية بالبكسل."

#: ../data/org.gnome.klotski.gschema.xml.h:4
#, fuzzy
msgid "Height of the window in pixels"
msgstr "ارتفاع النافذة الرئيسية بالبكسل."

#: ../data/org.gnome.klotski.gschema.xml.h:5
msgid "true if the window is maximized"
msgstr ""

#: ../src/gnome-klotski.vala:17
msgid "Print release version and exit"
msgstr ""

#: ../src/gnome-klotski.vala:114
#, fuzzy
msgid "Sliding block puzzles"
msgstr "قم بإزاحة المكعبات لتحل الأحجية"

#: ../src/gnome-klotski.vala:122
msgid "translator-credits"
msgstr ""
"فريق عربآيز للترجمة http://www.arabeyes.org :\n"
"عرفات المديني\t<silverwhale@silverpen.de>\n"
"سيّد جعفر الموسوي\t<mosawi@arabeyes.org>\n"
"عصام بايزيدي\t<bayazidi@arabeyes.org>\n"
"حسن عابدين\t<habdin@link.net>\n"
"عبدالعزيز العرفج\t<alarfaj0@yahoo.com>\n"
"جهاد عفيفي\t<djihed@gmail.com>\n"
"خالد حسني\t<khaledhosny@eglug.org>\n"
"أحمد فرغل\t<ahmad.farghal@gmail.com>\n"
"أنس الحسيني\t<linux.anas@gmail.com>\n"
"عبد المنعم كوكة\t<abdelmonam.kouka@ubuntume.com>\n"
"أنس عفيف عماد\t<anas.e87@gmail.com>\n"
"إبراهيم سعيد\t<ibraheem5000@gmail.com>"

#. puzzle name
#: ../src/klotski-window.vala:74
msgid "Only 18 Steps"
msgstr "18 خطوة فقط"

#. puzzle name
#: ../src/klotski-window.vala:87
msgid "Daisy"
msgstr "دايزي"

#. puzzle name
#: ../src/klotski-window.vala:100
msgid "Violet"
msgstr "بنفسجي"

#. puzzle name
#: ../src/klotski-window.vala:113
msgid "Poppy"
msgstr "بوبي"

#. puzzle name
#: ../src/klotski-window.vala:126
msgid "Pansy"
msgstr "بانسي"

#. puzzle name
#: ../src/klotski-window.vala:139
msgid "Snowdrop"
msgstr "سنو دروب"

#. puzzle name - sometimes called "Le'Ane Rouge"
#: ../src/klotski-window.vala:152
msgid "Red Donkey"
msgstr "الحمار الأحمر"

#. puzzle name
#: ../src/klotski-window.vala:165
msgid "Trail"
msgstr "ذيل"

#. puzzle name
#: ../src/klotski-window.vala:178
msgid "Ambush"
msgstr "دفع"

#. puzzle name
#: ../src/klotski-window.vala:191
msgid "Agatka"
msgstr "أجاتكا"

#. puzzle name
#: ../src/klotski-window.vala:202
msgid "Success"
msgstr "نجاح"

#. puzzle name
#: ../src/klotski-window.vala:212
msgid "Bone"
msgstr "عظم"

#. puzzle name
#: ../src/klotski-window.vala:225
msgid "Fortune"
msgstr "كنز"

#. puzzle name
#: ../src/klotski-window.vala:239
msgid "Fool"
msgstr "فوول"

#. puzzle name
#: ../src/klotski-window.vala:249
msgid "Solomon"
msgstr "سليمان"

#. puzzle name
#: ../src/klotski-window.vala:262
msgid "Cleopatra"
msgstr "كليوباترا"

#. puzzle name
#: ../src/klotski-window.vala:274
msgid "Shark"
msgstr "قرش"

#. puzzle name
#: ../src/klotski-window.vala:286
msgid "Rome"
msgstr "روما"

#. puzzle name
#: ../src/klotski-window.vala:298
msgid "Pennant Puzzle"
msgstr "لغز بنت"

#. puzzle name
#: ../src/klotski-window.vala:311
msgid "Ithaca"
msgstr "إيثيكا"

#. puzzle name
#: ../src/klotski-window.vala:334
msgid "Pelopones"
msgstr "بيلوبونيس"

#. puzzle name
#: ../src/klotski-window.vala:346
msgid "Transeuropa"
msgstr "ترانسيوروبا"

#. puzzle name
#: ../src/klotski-window.vala:358
msgid "Lodzianka"
msgstr "لودزيانكا"

#. puzzle name
#: ../src/klotski-window.vala:369
msgid "Polonaise"
msgstr "بولونايز"

#. puzzle name
#: ../src/klotski-window.vala:380
msgid "Baltic Sea"
msgstr "البحر البلطيقي"

#. puzzle name
#: ../src/klotski-window.vala:392
msgid "American Pie"
msgstr "الفطيرة الأمريكية"

#. puzzle name
#: ../src/klotski-window.vala:408
msgid "Traffic Jam"
msgstr "زحمة طريق"

#. puzzle name
#: ../src/klotski-window.vala:419
msgid "Sunshine"
msgstr "أشعة الشمس"

#. Label on the scores dialog, next to dropdown */
#: ../src/klotski-window.vala:527
#, fuzzy
msgid "Puzzle"
msgstr "الأحجية:"

#: ../src/klotski-window.vala:781
#, c-format
msgid "Moves: %d"
msgstr "نقلات: %Id"

#: ../src/klotski-window.vala:784
msgid "Level completed."
msgstr "اكتملت المرحلة."
