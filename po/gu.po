# translation of gnome-games.master.gu.po to Gujarati
# Ankit Patel <ankit644@yahoo.com>, 2005, 2006.
# Ankit Patel <ankit@redhat.com>, 2005, 2007, 2008.
# Sweta Kothari <swkothar@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: gnome-games.master.gu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-01 23:37+0100\n"
"PO-Revision-Date: 2009-09-16 11:39+0530\n"
"Last-Translator: Sweta Kothari <swkothar@redhat.com>\n"
"Language-Team: Gujarati\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"

#: ../data/gnome-klotski.appdata.xml.in.h:1
#, fuzzy
msgid "GNOME Klotski"
msgstr "ક્લોત્સકી"

#: ../data/gnome-klotski.appdata.xml.in.h:2
#: ../data/gnome-klotski.desktop.in.h:2
msgid "Slide blocks to solve the puzzle"
msgstr "કોયડો ઉકેલવા માટે બ્લોક સરકાવો"

#: ../data/gnome-klotski.appdata.xml.in.h:3
msgid ""
"GNOME Klotski is a set of block sliding puzzles. The objective is to move "
"the patterned block to the area bordered by green markers. To do so, you’ll "
"need to slide other blocks out of the way. Complete each puzzle in as few "
"moves as possible!"
msgstr ""

#: ../data/gnome-klotski.appdata.xml.in.h:4
msgid ""
"GNOME Klotski comes with nearly thirty different puzzle layouts of varying "
"difficulty. Some of the layouts are similar and only the size of one or more "
"blocks differ. Other layouts have more variety."
msgstr ""

#: ../data/gnome-klotski.appdata.xml.in.h:5
msgid "The GNOME Project"
msgstr ""

#: ../data/gnome-klotski.desktop.in.h:1 ../data/klotski.ui.h:6
#: ../src/gnome-klotski.vala:66 ../src/gnome-klotski.vala:112
msgid "Klotski"
msgstr "ક્લોત્સકી"

#: ../data/gnome-klotski.desktop.in.h:3
msgid "game;strategy;logic;move;"
msgstr ""

#: ../data/klotski.ui.h:1
#, fuzzy
msgid "Huarong Trail"
msgstr "હુઆરોંગ ટ્રેઈલ"

#: ../data/klotski.ui.h:2
msgid "Challenge Pack"
msgstr "શરતી પેક"

#: ../data/klotski.ui.h:3
msgid "Skill Pack"
msgstr "આવડત પેક"

#: ../data/klotski.ui.h:4
#, fuzzy
msgid "Previous"
msgstr "પહેલાનો કોયડો"

#: ../data/klotski.ui.h:5
#, fuzzy
msgid "Next"
msgstr "આગળ:"

#: ../data/klotski.ui.h:7
#, fuzzy
msgid "_Start Over"
msgstr "શરૂ કરો (_S)"

# libgnomeui/gnome-app-helper.c:258
#: ../data/klotski.ui.h:8
#, fuzzy
msgid "Restart the current puzzle"
msgstr "વર્તમાન રમત પુનઃશરૂ કરો"

#: ../data/klotski.ui.h:9
#, fuzzy
msgid "_Change Puzzle"
msgstr "પેનાન્ટ કોયડો"

#: ../data/klotski.ui.h:10
#, fuzzy
msgid "Choose an other puzzle"
msgstr "ગુણ સ્લોટ પસંદ કરો."

# libgnomeui/gnome-scores.c:94
#: ../data/klotski-menus.ui.h:1
msgid "_Scores"
msgstr "ગુણ (_S)"

#: ../data/klotski-menus.ui.h:2
msgid "_Help"
msgstr "મદદ (_H)"

#: ../data/klotski-menus.ui.h:3
msgid "_About"
msgstr "વિશે (_A)"

#: ../data/klotski-menus.ui.h:4
#, fuzzy
msgid "_Quit"
msgstr "બંધ કરો"

#: ../data/org.gnome.klotski.gschema.xml.h:1
msgid "The puzzle in play"
msgstr "રમતમાં કોયડો"

#: ../data/org.gnome.klotski.gschema.xml.h:2
msgid "The number of the puzzle being played."
msgstr "રમાયેલ કોયડાઓની સંખ્યા."

#: ../data/org.gnome.klotski.gschema.xml.h:3
#, fuzzy
msgid "Width of the window in pixels"
msgstr "પિક્સેલોમાં મુખ્ય વિનડોની પહોળાઇ."

#: ../data/org.gnome.klotski.gschema.xml.h:4
#, fuzzy
msgid "Height of the window in pixels"
msgstr "મુખ્ય વિન્ડોની ઊંચાઈ પિક્સેલોમાં."

#: ../data/org.gnome.klotski.gschema.xml.h:5
msgid "true if the window is maximized"
msgstr ""

#: ../src/gnome-klotski.vala:17
msgid "Print release version and exit"
msgstr ""

#: ../src/gnome-klotski.vala:114
#, fuzzy
msgid "Sliding block puzzles"
msgstr "કોયડો ઉકેલવા માટે બ્લોક સરકાવો"

#: ../src/gnome-klotski.vala:122
msgid "translator-credits"
msgstr "અંકિત પટેલ <ankit644@yahoo.com>, શ્ર્વેતા કોઠારી <swkothar@redhat.com>"

#. puzzle name
#: ../src/klotski-window.vala:74
msgid "Only 18 Steps"
msgstr "માત્ર ૧૮ પગલાં"

#. puzzle name
#: ../src/klotski-window.vala:87
msgid "Daisy"
msgstr "ચક્રાકાર"

#. puzzle name
#: ../src/klotski-window.vala:100
msgid "Violet"
msgstr "જાંબલી"

#. puzzle name
#: ../src/klotski-window.vala:113
msgid "Poppy"
msgstr "પોપી"

#. puzzle name
#: ../src/klotski-window.vala:126
msgid "Pansy"
msgstr "પેન્સી"

#. puzzle name
#: ../src/klotski-window.vala:139
msgid "Snowdrop"
msgstr "સ્નોડ્રોપ"

#. puzzle name - sometimes called "Le'Ane Rouge"
#: ../src/klotski-window.vala:152
msgid "Red Donkey"
msgstr "લાલ વાંદરો"

#. puzzle name
#: ../src/klotski-window.vala:165
msgid "Trail"
msgstr "ટ્રેઈલ"

#. puzzle name
#: ../src/klotski-window.vala:178
msgid "Ambush"
msgstr "એમ્બશ"

#. puzzle name
#: ../src/klotski-window.vala:191
msgid "Agatka"
msgstr "એગાટ્કા"

# libgnomeui/gnome-scores.c:94
#. puzzle name
#: ../src/klotski-window.vala:202
msgid "Success"
msgstr "સફળતા"

#. puzzle name
#: ../src/klotski-window.vala:212
msgid "Bone"
msgstr "બોન"

#. puzzle name
#: ../src/klotski-window.vala:225
msgid "Fortune"
msgstr "સૌભાગ્ય"

#. puzzle name
#: ../src/klotski-window.vala:239
msgid "Fool"
msgstr "ફુલ"

#. puzzle name
#: ../src/klotski-window.vala:249
msgid "Solomon"
msgstr "સોલોમન"

#. puzzle name
#: ../src/klotski-window.vala:262
msgid "Cleopatra"
msgstr "ક્લેઓપેટ્રા"

#. puzzle name
#: ../src/klotski-window.vala:274
msgid "Shark"
msgstr "શાર્ક"

#. puzzle name
#: ../src/klotski-window.vala:286
msgid "Rome"
msgstr "રોમ"

#. puzzle name
#: ../src/klotski-window.vala:298
msgid "Pennant Puzzle"
msgstr "પેનાન્ટ કોયડો"

#. puzzle name
#: ../src/klotski-window.vala:311
msgid "Ithaca"
msgstr "ઈથાકા"

#. puzzle name
#: ../src/klotski-window.vala:334
msgid "Pelopones"
msgstr "પેલોપોનસ"

#. puzzle name
#: ../src/klotski-window.vala:346
msgid "Transeuropa"
msgstr "ટ્રાન્સયુરોપા"

#. puzzle name
#: ../src/klotski-window.vala:358
msgid "Lodzianka"
msgstr "લોડઝીઆન્કા"

#. puzzle name
#: ../src/klotski-window.vala:369
msgid "Polonaise"
msgstr "પોલોનાઈસ"

#. puzzle name
#: ../src/klotski-window.vala:380
msgid "Baltic Sea"
msgstr "બાલ્ટિક સમુદ્ર"

#. puzzle name
#: ../src/klotski-window.vala:392
msgid "American Pie"
msgstr "અમેરિકન પાઈ"

#. puzzle name
#: ../src/klotski-window.vala:408
msgid "Traffic Jam"
msgstr "ટ્રાફિક જામ"

#. puzzle name
#: ../src/klotski-window.vala:419
msgid "Sunshine"
msgstr "સનસાઈન"

#. Label on the scores dialog, next to dropdown */
#: ../src/klotski-window.vala:527
#, fuzzy
msgid "Puzzle"
msgstr "કોયડો:"

#: ../src/klotski-window.vala:781
#, c-format
msgid "Moves: %d"
msgstr "ચાલો: %d"

#: ../src/klotski-window.vala:784
msgid "Level completed."
msgstr "સ્તર પૂર્ણ."
