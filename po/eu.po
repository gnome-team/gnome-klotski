# Basque translation of gnome-klotski.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Iñaki Larrañaga Murgoitio <dooteo@zundan.com>, 2008, 2009, 2010, 2011, 2012, 2013.
# Iñaki Larrañaga Murgoitio <dooteo@zundan.com>, 2014, 2015, 2016.
# Asier Sarasua Garmendia  <asiersarasua@ni.eus>, 2013, 2019, 2020.
#
msgid ""
msgstr "Project-Id-Version: gnome-klotski master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-klotski/issues\n"
"POT-Creation-Date: 2020-07-29 12:49+0000\n"
"PO-Revision-Date: 2020-08-10 10:00+0100\n"
"Last-Translator: Asier Sarasua Garmendia <asiersarasua@ni.eus>\n"
"Language-Team: Basque <librezale@librezale.eus>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains "Toggle game menu" and "Restart"
#: data/ui/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Puzzle-related"
msgstr "Puzzleei lotutakoa"

#. Translators: Ctrl-Shift-N shortcut description in the Keyboard Shortcuts dialog, section "Puzzle-related"; toggles the game menu
#: data/ui/help-overlay.ui:36
msgctxt "shortcut window"
msgid "Toggle game menu"
msgstr "Txandakatu jokoaren menua"

#. Translators: Ctrl-Shift-N shortcut description in the Keyboard Shortcuts dialog, section "Puzzle-related"; restarts the current puzzle
#: data/ui/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Restart"
msgstr "Berrabiarazi"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains the actions for selecting a new puzzle
#: data/ui/help-overlay.ui:54
msgctxt "shortcut window"
msgid "Change Puzzle popover"
msgstr "Aldatu puzleen bunbuiloa"

#. Translators: Ctrl-S shortcut description in the Keyboard Shortcuts dialog, section "Headerbar"; toggles the Change Puzzle menu
#: data/ui/help-overlay.ui:59
msgctxt "shortcut window"
msgid "Select new puzzle"
msgstr "Hautatu puzzle berria"

#. Translators: Up action description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to previous puzzle
#: data/ui/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Previous puzzle"
msgstr "Aurreko puzzlea"

#. Translators: Down shortcut description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to next puzzle
#: data/ui/help-overlay.ui:75
msgctxt "shortcut window"
msgid "Next puzzle"
msgstr "Hurrengo puzzlea"

#. Translators: Page_Up action description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to previous puzzles group
#: data/ui/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Previous puzzles group"
msgstr "Aurreko puzzle-taldea"

#. Translators: Page_Down shortcut description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to next puzzles group
#: data/ui/help-overlay.ui:91
msgctxt "shortcut window"
msgid "Next puzzles group"
msgstr "Hurrengo puzzle-taldea"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains (only) "Scores dialog"
#: data/ui/help-overlay.ui:101
msgctxt "shortcut window"
msgid "Scores"
msgstr "Puntuazioak"

#. Translators: Ctrl-S shortcut description in the Keyboard Shortcuts dialog, section "Scores"
#: data/ui/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Scores dialog"
msgstr "Puntuazioen elkarrizketa-koadroa"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains "Help", "About", "Quit"...
#: data/ui/help-overlay.ui:116
msgctxt "shortcut window"
msgid "Generic"
msgstr "Orokorra"

#. Translators: F10 shortcut description in the Keyboard Shortcuts dialog, section "Generic": toggles the hamburger menu
#: data/ui/help-overlay.ui:121
msgctxt "shortcut window"
msgid "Toggle main menu"
msgstr "Txandakatu menu nagusia"

#. Translators: Ctrl-? shortcut description in the Keyboard Shortcuts dialog, section "Generic": opens Shortcuts dialog
#: data/ui/help-overlay.ui:129
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Laster-teklak"

#. Translators: F1 shortcut description in the Keyboard Shortcuts dialog, section "Generic": opens the application help
#: data/ui/help-overlay.ui:137
msgctxt "shortcut window"
msgid "Help"
msgstr "Laguntza"

#. Translators: Shift-F1 shortcut description in the Keyboard Shortcuts dialog, section "Generic": shows the About dialog
#: data/ui/help-overlay.ui:145
msgctxt "shortcut window"
msgid "About"
msgstr "Honi buruz"

#. Translators: Ctrl-Q shortcut description in the Keyboard Shortcuts dialog, section "Generic": quits the application
#: data/ui/help-overlay.ui:153
msgctxt "shortcut window"
msgid "Quit"
msgstr "Irten"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the Scores dialog
#: data/ui/klotski.ui:24
msgid "_Scores"
msgstr "_Puntuazioak"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the Keyboard Shortcuts dialog
#: data/ui/klotski.ui:31
msgid "_Keyboard Shortcuts"
msgstr "Las_ter-teklak"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the help of the application
#: data/ui/klotski.ui:36
msgid "_Help"
msgstr "_Laguntza"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the About dialog
#: data/ui/klotski.ui:41
msgid "_About Klotski"
msgstr "Klotski jokoari _buruz"

#. Translators: label of an entry in the game menu (with a mnemonic that appears pressing Alt); allows to directly restart the current puzzle
#: data/ui/klotski.ui:50
msgid "_Start Over"
msgstr "_Hasi hasieratik"

#. Translators: name of a levels pack, as seen in the Change Puzzle popover; contains classical levels
#: data/ui/klotski.ui:94
msgid "Huarong Trail"
msgstr "Huarong bidea"

#. Translators: name of a levels pack, as seen in the Change Puzzle popover; contains medium-difficulty levels
#: data/ui/klotski.ui:106
msgid "Challenge Pack"
msgstr "Erronka paketea"

#. Translators: name of a levels pack, as seen in the Change Puzzle popover; contains hardest levels
#: data/ui/klotski.ui:118
msgid "Skill Pack"
msgstr "Trebetasuna paketea"

#. Translators: in the Change Puzzle popover, label of the button for playing the previous level
#: data/ui/klotski.ui:300
msgid "Previous"
msgstr "Aurrekoa"

#. Translators: in the Change Puzzle popover, label of the button for playing the next level
#: data/ui/klotski.ui:313
msgid "Next"
msgstr "Hurrengoa"

#. Translators: application name, as used in the window manager, the window title, the about dialog...
#: data/ui/klotski.ui:328 data/ui/klotski.ui:339
#: data/org.gnome.Klotski.desktop.in:3 src/gnome-klotski.vala:25
msgid "Klotski"
msgstr "Klotski"

#. Translators: in the headerbar, button that allows via a popover to select a different puzzle (with a mnemonic that appears pressing Alt)
#: data/ui/klotski.ui:346
msgid "_Change Puzzle"
msgstr "_Aldatu jokoa"

#. Translators: tooltip text of the Change Puzzle menubutton, in the headerbar
#: data/ui/klotski.ui:351
msgid "Choose an other puzzle"
msgstr "Aukeratu beste puzzlea"

#. Translators: tooltip text of the Start Over button, in the headerbar
#: data/ui/klotski.ui:364
msgid "Moves counter"
msgstr "Zenbatzailea mugitzen du"

#: data/org.gnome.Klotski.appdata.xml.in:7
msgid "GNOME Klotski"
msgstr "GNOME Klotski"

#: data/org.gnome.Klotski.appdata.xml.in:8 data/org.gnome.Klotski.desktop.in:4
msgid "Slide blocks to solve the puzzle"
msgstr "Arrastatu blokeak buru-hausgarria ebazteko"

#: data/org.gnome.Klotski.appdata.xml.in:10
msgid ""
"GNOME Klotski is a set of block sliding puzzles. The objective is to move "
"the patterned block to the area bordered by green markers. To do so, you’ll "
"need to slide other blocks out of the way. Complete each puzzle in as few "
"moves as possible!"
msgstr "GNOME Klotski blokeak korritzeko puzzle multzo bat da. Markak dituen blokea berde koloreko markatzaileak dauden lekura eramatea da helburua. Hori lortzeko beste blokeak trabatik kendu beharko dituzu. Osatu puzzle bakoitza ahalik eta mugimendu gutxienekin."

#: data/org.gnome.Klotski.appdata.xml.in:16
msgid ""
"GNOME Klotski comes with nearly thirty different puzzle layouts of varying "
"difficulty. Some of the layouts are similar and only the size of one or more "
"blocks differ. Other layouts have more variety."
msgstr "GNOME Klotski hogeita hamar diseinu eta zailtasun desberdineko puzzle inguru ditu. Diseinu batzuk antzekoak dira eta bloke baten edo gehiagoren tamainak aldatzen dira. Beste diseinu batzuk aldaera gehiago dituzte."

#: data/org.gnome.Klotski.appdata.xml.in:44
msgid "The GNOME Project"
msgstr "GNOME proiektua"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Klotski.desktop.in:6
msgid "game;strategy;logic;move;"
msgstr "jokoa;estrategia;logika;mugimendua;"

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Klotski/level'
#: data/org.gnome.Klotski.gschema.xml:6
msgid "The puzzle in play"
msgstr "Jokoan dagoen buru-hausgarria"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Klotski/level'
#: data/org.gnome.Klotski.gschema.xml:8
msgid "The number of the puzzle being played."
msgstr "Jokoan dauden buru-hausgarri kopurua."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Klotski/window-width'
#: data/org.gnome.Klotski.gschema.xml:13
msgid "The width of the window"
msgstr "Leihoaren zabalera"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Klotski/window-width'
#: data/org.gnome.Klotski.gschema.xml:15
msgid "Width of the window in pixels"
msgstr "Leihoaren zabalera (pixeletan)"

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Klotski/window-height'
#: data/org.gnome.Klotski.gschema.xml:20
msgid "The height of the window"
msgstr "Leihoaren altuera"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Klotski/window-height'
#: data/org.gnome.Klotski.gschema.xml:22
msgid "Height of the window in pixels"
msgstr "Leihoaren altuera (pixeletan)"

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/Klotski/window-is-maximized'
#: data/org.gnome.Klotski.gschema.xml:27
msgid "A flag to enable maximized mode"
msgstr "Modu maximizatua gaitzeko bandera"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/Klotski/window-is-maximized'
#: data/org.gnome.Klotski.gschema.xml:29
msgid "If “true”, the main window starts in maximized mode."
msgstr "Egia bada, leiho nagusia modu maximizatuan abiaraziko da."

#. Translators: command-line option description, see 'gnome-klotski --help'
#: src/gnome-klotski.vala:32
msgid "Print release version and exit"
msgstr "Erakutsi bertsioaren informazioa eta irten"

#. Translators: puzzle name
#: src/klotski-window.vala:91
msgid "Only 18 Steps"
msgstr "Soilik 18 urrats"

#. Translators: puzzle name
#: src/klotski-window.vala:104
msgid "Daisy"
msgstr "Bitxilorea"

#. Translators: puzzle name
#: src/klotski-window.vala:117
msgid "Violet"
msgstr "Bioleta"

#. Translators: puzzle name
#: src/klotski-window.vala:130
msgid "Poppy"
msgstr "Mitxoleta"

#. Translators: puzzle name
#: src/klotski-window.vala:143
msgid "Pansy"
msgstr "Pentsamendua"

#. Translators: puzzle name
#: src/klotski-window.vala:156
msgid "Snowdrop"
msgstr "Negu-txilintxa"

#. Translators: puzzle name — sometimes called by french name "L’Âne Rouge" (with the same meaning)
#: src/klotski-window.vala:169
msgid "Red Donkey"
msgstr "Asto gorria"

#. Translators: puzzle name
#: src/klotski-window.vala:182
msgid "Trail"
msgstr "Aztarna"

#. Translators: puzzle name
#: src/klotski-window.vala:195
msgid "Ambush"
msgstr "Segada"

#. Translators: puzzle name
#: src/klotski-window.vala:208
msgid "Agatka"
msgstr "Agatka"

#. Translators: puzzle name
#: src/klotski-window.vala:219
msgid "Success"
msgstr "Ongi burutu da"

#. Translators: puzzle name
#: src/klotski-window.vala:229
msgid "Bone"
msgstr "Hezurra"

#. Translators: puzzle name
#: src/klotski-window.vala:242
msgid "Fortune"
msgstr "Zortea"

#. Translators: puzzle name
#: src/klotski-window.vala:256
msgid "Fool"
msgstr "Eroa"

#. Translators: puzzle name
#: src/klotski-window.vala:266
msgid "Solomon"
msgstr "Salomon"

#. Translators: puzzle name
#: src/klotski-window.vala:279
msgid "Cleopatra"
msgstr "Kleopatra"

#. Translators: puzzle name
#: src/klotski-window.vala:291
msgid "Shark"
msgstr "Marrazoa"

#. Translators: puzzle name
#: src/klotski-window.vala:303
msgid "Rome"
msgstr "Erroma"

#. Translators: puzzle name
#: src/klotski-window.vala:315
msgid "Pennant Puzzle"
msgstr "Pennant buru-hausgarria"

#. Translators: puzzle name
#: src/klotski-window.vala:328
msgid "Ithaca"
msgstr "Ithaca"

#. Translators: puzzle name
#: src/klotski-window.vala:351
msgid "Pelopones"
msgstr "Peloponeso"

#. Translators: puzzle name
#: src/klotski-window.vala:363
msgid "Transeuropa"
msgstr "Transeuropa"

#. Translators: puzzle name
#: src/klotski-window.vala:375
msgid "Lodzianka"
msgstr "Lodzianka"

#. Translators: puzzle name
#: src/klotski-window.vala:386
msgid "Polonaise"
msgstr "Polonesa"

#. Translators: puzzle name
#: src/klotski-window.vala:397
msgid "Baltic Sea"
msgstr "Itsaso Baltikoa"

#. Translators: puzzle name
#: src/klotski-window.vala:409
msgid "American Pie"
msgstr "Pastel amerikarra"

#. Translators: puzzle name
#: src/klotski-window.vala:425
msgid "Traffic Jam"
msgstr "Buxadura"

#. Translators: puzzle name
#: src/klotski-window.vala:436
msgid "Sunshine"
msgstr "Eguzki-izpia"

#. Translators: in the Scores dialog, label indicating for which puzzle the best scores are displayed
#: src/klotski-window.vala:554
msgid "Puzzle"
msgstr "Puzzlea"

#. Translators: headerbar title, when the puzzle is solved
#: src/klotski-window.vala:894
msgid "Level completed."
msgstr "Maila osatu da."

#. Translators: text crediting an author, in the about dialog
#: src/klotski-window.vala:1119
msgid "Lars Rydlinge (original author)"
msgstr "Lars Rydlinge (jatorrizko egilea)"

#. Translators: text crediting an author, in the about dialog
#: src/klotski-window.vala:1123
msgid "Robert Ancell (port to vala)"
msgstr "Robert Ancell (vala lengoaiara eramatea)"

#. Translators: text crediting an author, in the about dialog
#: src/klotski-window.vala:1127
msgid "John Cheetham (port to vala)"
msgstr "John Cheetham (vala lengoaiara eramatea)"

#. Translators: text crediting a documenter, in the about dialog
#: src/klotski-window.vala:1131
msgid "Andrew Sobala"
msgstr "Andrew Sobala"

#. Translators: small description of the game, seen in the About dialog
#: src/klotski-window.vala:1139
msgid "Sliding block puzzles"
msgstr "Puzzle blokea desplazatzea"

#. Translators: text crediting a maintainer, seen in the About dialog
#: src/klotski-window.vala:1143
msgid "Copyright © 1999-2008 – Lars Rydlinge"
msgstr "Copyright © 1999-2008 – Lars Rydlinge"

#. Translators: text crediting a maintainer, seen in the About dialog; the %u are replaced with the years of start and end
#: src/klotski-window.vala:1147
#, c-format
msgid "Copyright © %u-%u – Michael Catanzaro"
msgstr "Copyright © %u-%u – Michael Catanzaro"

#. Translators: text crediting a maintainer, seen in the About dialog; the %u are replaced with the years of start and end
#: src/klotski-window.vala:1151
#, c-format
msgid "Copyright © %u-%u – Arnaud Bonatti"
msgstr "Copyright © %u-%u – Arnaud Bonatti"

#. Translators: about dialog text; this string should be replaced by a text crediting yourselves and your translation team, or should be left empty. Do not translate literally!
#: src/klotski-window.vala:1157
msgid "translator-credits"
msgstr "translator-credits"

#~ msgid "Restart the current puzzle"
#~ msgstr "Berrabiarazi uneko jokoa"

#~ msgid "Moves: %d"
#~ msgstr "Mugimenduak: %d"
