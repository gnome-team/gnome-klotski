# Danish translation of gnome-klotski.
# Copyright (C) 1998-2016, 2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-klotski package.
# Anders Wegge Jakobsen <wegge@wegge.dk>, 1998.
# Kenneth Christiansen <kenneth@ripen.dk>, 1998-2001.
# Keld Simonsen <keld@dkuug.dk>, 2000-2002.
# Ole Laursen <olau@hardworking.dk>, 2001, 02, 03, 04.
# Martin Willemoes Hansen <mwh@sysrq.dk>, 2004, 05.
# flemming christensen <fc@stromata.dk>, 2011.
# Ask Hjorth Larsen <asklarsen@gmail.com>, 2007-2016, 2019.
# scootergrisen, 2020.
#
# Husk at tilføje dig i credit-listen (besked id "translator-credits")
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-klotski master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-klotski/issues\n"
"POT-Creation-Date: 2020-05-06 16:36+0000\n"
"PO-Revision-Date: 2020-05-08 13:27+0200\n"
"Last-Translator: scootergrisen\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains "Toggle game menu" and "Restart"
#: data/ui/help-overlay.ui:31
#| msgid "Puzzle"
msgctxt "shortcut window"
msgid "Puzzle-related"
msgstr "Opgaverelaterede"

#. Translators: Ctrl-Shift-N shortcut description in the Keyboard Shortcuts dialog, section "Puzzle-related"; toggles the game menu
#: data/ui/help-overlay.ui:36
msgctxt "shortcut window"
msgid "Toggle game menu"
msgstr "Spilmenu til/fra"

#. Translators: Ctrl-Shift-N shortcut description in the Keyboard Shortcuts dialog, section "Puzzle-related"; restarts the current puzzle
#: data/ui/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Restart"
msgstr "Genstart"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains the actions for selecting a new puzzle
#: data/ui/help-overlay.ui:54
#| msgid "_Change Puzzle"
msgctxt "shortcut window"
msgid "Change Puzzle popover"
msgstr "Popover for skift opgave"

#. Translators: Ctrl-S shortcut description in the Keyboard Shortcuts dialog, section "Headerbar"; toggles the Change Puzzle menu
#: data/ui/help-overlay.ui:59
msgctxt "shortcut window"
msgid "Select new puzzle"
msgstr "Vælg ny opgave"

#. Translators: Up action description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to previous puzzle
#: data/ui/help-overlay.ui:67
#| msgid "Previous"
msgctxt "shortcut window"
msgid "Previous puzzle"
msgstr "Forrige opgave"

#. Translators: Down shortcut description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to next puzzle
#: data/ui/help-overlay.ui:75
#| msgid "Pennant Puzzle"
msgctxt "shortcut window"
msgid "Next puzzle"
msgstr "Næste opgave"

#. Translators: Page_Up action description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to previous puzzles group
#: data/ui/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Previous puzzles group"
msgstr "Forrige gruppe med opgaver"

#. Translators: Page_Down shortcut description in the Keyboard Shortcuts dialog, section "Change Puzzle popover"; switch to next puzzles group
#: data/ui/help-overlay.ui:91
msgctxt "shortcut window"
msgid "Next puzzles group"
msgstr "Næste gruppe med opgaver"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains (only) "Scores dialog"
#: data/ui/help-overlay.ui:101
#| msgid "_Scores"
msgctxt "shortcut window"
msgid "Scores"
msgstr "Resultater"

#. Translators: Ctrl-S shortcut description in the Keyboard Shortcuts dialog, section "Scores"
#: data/ui/help-overlay.ui:106
#| msgid "_Scores"
msgctxt "shortcut window"
msgid "Scores dialog"
msgstr "Dialogen Resultater"

#. Translators: title of a section in the Keyboard Shortcuts dialog; contains "Help", "About", "Quit"...
#: data/ui/help-overlay.ui:116
msgctxt "shortcut window"
msgid "Generic"
msgstr "Generisk"

#. Translators: F10 shortcut description in the Keyboard Shortcuts dialog, section "Generic": toggles the hamburger menu
#: data/ui/help-overlay.ui:121
msgctxt "shortcut window"
msgid "Toggle main menu"
msgstr "Hovedmenu til/fra"

#. Translators: Ctrl-? shortcut description in the Keyboard Shortcuts dialog, section "Generic": opens Shortcuts dialog
#: data/ui/help-overlay.ui:129
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Tastaturgenveje"

#. Translators: F1 shortcut description in the Keyboard Shortcuts dialog, section "Generic": opens the application help
#: data/ui/help-overlay.ui:137
#| msgid "_Help"
msgctxt "shortcut window"
msgid "Help"
msgstr "Hjælp"

#. Translators: Shift-F1 shortcut description in the Keyboard Shortcuts dialog, section "Generic": shows the About dialog
#: data/ui/help-overlay.ui:145
#| msgid "_About"
msgctxt "shortcut window"
msgid "About"
msgstr "Om"

#. Translators: Ctrl-Q shortcut description in the Keyboard Shortcuts dialog, section "Generic": quits the application
#: data/ui/help-overlay.ui:153
#| msgid "_Quit"
msgctxt "shortcut window"
msgid "Quit"
msgstr "Afslut"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the Scores dialog
#: data/ui/klotski.ui:24
msgid "_Scores"
msgstr "_Resultater"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the Keyboard Shortcuts dialog
#: data/ui/klotski.ui:31
msgid "_Keyboard Shortcuts"
msgstr "_Tastaturgenveje"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the help of the application
#: data/ui/klotski.ui:36
msgid "_Help"
msgstr "_Hjælp"

#. Translators: label of an entry in the Hamburger menu (with a mnemonic that appears pressing Alt); opens the About dialog
#: data/ui/klotski.ui:41
msgid "_About Klotski"
msgstr "_Om Klotski"

#. Translators: label of an entry in the game menu (with a mnemonic that appears pressing Alt); allows to directly restart the current puzzle
#: data/ui/klotski.ui:50
msgid "_Start Over"
msgstr "_Begynd forfra"

#. Translators: name of a levels pack, as seen in the Change Puzzle popover; contains classical levels
#: data/ui/klotski.ui:94
msgid "Huarong Trail"
msgstr "Huarong-sporet"

#. Translators: name of a levels pack, as seen in the Change Puzzle popover; contains medium-difficulty levels
#: data/ui/klotski.ui:106
msgid "Challenge Pack"
msgstr "Udfordringspakken"

#. Translators: name of a levels pack, as seen in the Change Puzzle popover; contains hardest levels
#: data/ui/klotski.ui:118
msgid "Skill Pack"
msgstr "Færdighedspakken"

#. Translators: in the Change Puzzle popover, label of the button for playing the previous level
#: data/ui/klotski.ui:300
msgid "Previous"
msgstr "Forrige"

#. Translators: in the Change Puzzle popover, label of the button for playing the next level
#: data/ui/klotski.ui:313
msgid "Next"
msgstr "Næste"

#. Translators: application name, as used in the window manager, the window title, the about dialog...
#: data/ui/klotski.ui:328 data/ui/klotski.ui:339
#: data/org.gnome.Klotski.desktop.in:3 src/gnome-klotski.vala:25
msgid "Klotski"
msgstr "Klotski"

#. Translators: in the headerbar, button that allows via a popover to select a different puzzle (with a mnemonic that appears pressing Alt)
#: data/ui/klotski.ui:346
msgid "_Change Puzzle"
msgstr "_Skift opgave"

#. Translators: tooltip text of the Change Puzzle menubutton, in the headerbar
#: data/ui/klotski.ui:351
msgid "Choose an other puzzle"
msgstr "Vælg en anden opgave"

#. Translators: tooltip text of the Start Over button, in the headerbar
#: data/ui/klotski.ui:364
msgid "Moves counter"
msgstr "Træktæller"

#: data/org.gnome.Klotski.appdata.xml.in:7
msgid "GNOME Klotski"
msgstr "GNOME Klotski"

#: data/org.gnome.Klotski.appdata.xml.in:8 data/org.gnome.Klotski.desktop.in:4
msgid "Slide blocks to solve the puzzle"
msgstr "Flyt blokkene for at løse opgaven"

#: data/org.gnome.Klotski.appdata.xml.in:10
msgid ""
"GNOME Klotski is a set of block sliding puzzles. The objective is to move "
"the patterned block to the area bordered by green markers. To do so, you’ll "
"need to slide other blocks out of the way. Complete each puzzle in as few "
"moves as possible!"
msgstr ""
"GNOME Klotski er et tænkespil, hvor man flytter blokke. Målet er at "
"flytte blokken med mønsteret til området med de grønne markører. For at "
"kunne gøre det, skal du flytte andre blokke af vejen. Gennemfør hver "
"opgave med så få træk som muligt!"

#: data/org.gnome.Klotski.appdata.xml.in:16
msgid ""
"GNOME Klotski comes with nearly thirty different puzzle layouts of varying "
"difficulty. Some of the layouts are similar and only the size of one or more "
"blocks differ. Other layouts have more variety."
msgstr ""
"GNOME Klotski indeholder næsten tredive forskellige opgaver af varierende "
"sværhedsgrad. Nogle minder meget om hinanden, idet kun størrelsen af en "
"eller flere blokke varierer. Andre har større variation."

#: data/org.gnome.Klotski.appdata.xml.in:44
msgid "The GNOME Project"
msgstr "GNOME-projektet"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Klotski.desktop.in:6
msgid "game;strategy;logic;move;"
msgstr "spil;strategi;logik;flyt;træk;flytte;trække;"

#: data/org.gnome.Klotski.gschema.xml:5
msgid "The puzzle in play"
msgstr "Det igangværende spil"

#: data/org.gnome.Klotski.gschema.xml:6
msgid "The number of the puzzle being played."
msgstr "Spillets nummer."

#: data/org.gnome.Klotski.gschema.xml:10
msgid "Width of the window in pixels"
msgstr "Bredden af vinduet i billedpunkter"

#: data/org.gnome.Klotski.gschema.xml:14
msgid "Height of the window in pixels"
msgstr "Højden af vinduet i billedpunkter"

#: data/org.gnome.Klotski.gschema.xml:18
msgid "true if the window is maximized"
msgstr "sand hvis vinduet er maksimeret"

#. Translators: command-line option description, see 'gnome-klotski --help'
#: src/gnome-klotski.vala:32
msgid "Print release version and exit"
msgstr "Vis udgivelsesversion og afslut"

#. Translators: puzzle name
#: src/klotski-window.vala:91
msgid "Only 18 Steps"
msgstr "Kun 18 skridt"

#. Translators: puzzle name
#: src/klotski-window.vala:104
msgid "Daisy"
msgstr "Tusindfryd"

#. Translators: puzzle name
#: src/klotski-window.vala:117
msgid "Violet"
msgstr "Viol"

#. Translators: puzzle name
#: src/klotski-window.vala:130
msgid "Poppy"
msgstr "Valmue"

#. Translators: puzzle name
#: src/klotski-window.vala:143
msgid "Pansy"
msgstr "Stedmoderblomst"

#. Translators: puzzle name
#: src/klotski-window.vala:156
msgid "Snowdrop"
msgstr "Vintergæk"

#. puzzle name - sometimes called "Le'Ane Rouge"
#: src/klotski-window.vala:169
msgid "Red Donkey"
msgstr "Det røde æsel"

#. Translators: puzzle name
#: src/klotski-window.vala:182
msgid "Trail"
msgstr "Kæde"

#. Translators: puzzle name
#: src/klotski-window.vala:195
msgid "Ambush"
msgstr "Bagholdsangreb"

#. Translators: puzzle name
#: src/klotski-window.vala:208
msgid "Agatka"
msgstr "Agatka"

#. Translators: puzzle name
#: src/klotski-window.vala:219
msgid "Success"
msgstr "Succes"

#. Translators: puzzle name
#: src/klotski-window.vala:229
msgid "Bone"
msgstr "Knogle"

#. Translators: puzzle name
#: src/klotski-window.vala:242
msgid "Fortune"
msgstr "Lykke"

#. Translators: puzzle name
#: src/klotski-window.vala:256
msgid "Fool"
msgstr "Fjols"

#. Translators: puzzle name
#: src/klotski-window.vala:266
msgid "Solomon"
msgstr "Salomon"

#. Translators: puzzle name
#: src/klotski-window.vala:279
msgid "Cleopatra"
msgstr "Kleopatra"

#. Translators: puzzle name
#: src/klotski-window.vala:291
msgid "Shark"
msgstr "Haj"

#. Translators: puzzle name
#: src/klotski-window.vala:303
msgid "Rome"
msgstr "Rom"

#. Translators: puzzle name
#: src/klotski-window.vala:315
msgid "Pennant Puzzle"
msgstr "Vimpel"

#. Translators: puzzle name
#: src/klotski-window.vala:328
msgid "Ithaca"
msgstr "Ithaca"

#. Translators: puzzle name
#: src/klotski-window.vala:351
msgid "Pelopones"
msgstr "Pelopones"

#. Translators: puzzle name
#: src/klotski-window.vala:363
msgid "Transeuropa"
msgstr "Transeuropa"

#. Translators: puzzle name
#: src/klotski-window.vala:375
msgid "Lodzianka"
msgstr "Lodzianka"

#. Translators: puzzle name
#: src/klotski-window.vala:386
msgid "Polonaise"
msgstr "Polonaise"

#. Translators: puzzle name
#: src/klotski-window.vala:397
msgid "Baltic Sea"
msgstr "Det baltiske hav"

#. Translators: puzzle name
#: src/klotski-window.vala:409
msgid "American Pie"
msgstr "Amerikansk tærte"

#. Translators: puzzle name
#: src/klotski-window.vala:425
msgid "Traffic Jam"
msgstr "Trafikprop"

#. Translators: puzzle name
#: src/klotski-window.vala:436
msgid "Sunshine"
msgstr "Solskin"

#. Translators: in the Scores dialog, label indicating for which puzzle the best scores are displayed
#: src/klotski-window.vala:554
msgid "Puzzle"
msgstr "Opgave"

#. Translators: headerbar title, when the puzzle is solved
#: src/klotski-window.vala:893
msgid "Level completed."
msgstr "Bane fuldført."

#. Translators: text crediting an author, in the about dialog
#: src/klotski-window.vala:1118
msgid "Lars Rydlinge (original author)"
msgstr "Lars Rydlinge (oprindelig forfatter)"

#. Translators: text crediting an author, in the about dialog
#: src/klotski-window.vala:1122
msgid "Robert Ancell (port to vala)"
msgstr "Robert Ancell (portering til vala)"

#. Translators: text crediting an author, in the about dialog
#: src/klotski-window.vala:1126
msgid "John Cheetham (port to vala)"
msgstr "John Cheetham (portering til vala)"

#. Translators: text crediting a documenter, in the about dialog
#: src/klotski-window.vala:1130
msgid "Andrew Sobala"
msgstr "Andrew Sobala"

#. Translators: small description of the game, seen in the About dialog
#: src/klotski-window.vala:1138
msgid "Sliding block puzzles"
msgstr "Opgaver med blokke der kan flyttes"

#. Translators: text crediting a maintainer, seen in the About dialog
#: src/klotski-window.vala:1142
msgid "Copyright © 1999-2008 – Lars Rydlinge"
msgstr "Ophavsret © 1999–2008 – Lars Rydlinge"

#. Translators: text crediting a maintainer, seen in the About dialog; the %u are replaced with the years of start and end
#: src/klotski-window.vala:1146
#, c-format
msgid "Copyright © %u-%u – Michael Catanzaro"
msgstr "Ophavsret © %u–%u – Michael Catanzaro"

#. Translators: text crediting a maintainer, seen in the About dialog; the %u are replaced with the years of start and end
#: src/klotski-window.vala:1150
#, c-format
msgid "Copyright © %u-%u – Arnaud Bonatti"
msgstr "Ophavsret © %u–%u – Arnaud Bonatti"

#. Translators: about dialog text; this string should be replaced by a text crediting yourselves and your translation team, or should be left empty. Do not translate literally!
#: src/klotski-window.vala:1156
msgid "translator-credits"
msgstr ""
"Anders Wegge Jakobsen\n"
"Keld Simonsen\n"
"Kenneth Christiansen\n"
"Martin Willemoes Hansen\n"
"Ole Laursen\n"
"Ask Hjorth Larsen\n"
"scootergrisen\n"
"\n"
"Dansk-gruppen\n"
"Websted http://dansk-gruppen.dk\n"
"E-mail <dansk@dansk-gruppen.dk>"
