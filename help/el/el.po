# Jennie Petoumenou <epetoumenou@gmail.com>, 2009.
# Fotis Tsamis <ftsamis@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: packagename\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-08-05 09:25+0000\n"
"PO-Revision-Date: 2012-08-23 13:59+0200\n"
"Last-Translator: Tom Tryfonidis <tomtryf@gmail.com>\n"
"Language-Team: Greek <team@gnome.gr>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

# άλλαξα λίγο την διατύπωση
#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Ελληνική μεταφραστική ομάδα GNOME\n"
" Φώτης Τσάμης <ftsamis@gmail.com>, 2009\n"
"\n"
"Για περισσότερα δείτε http://www.gnome.gr/"

#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#.
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/index.docbook:204(imagedata)
msgctxt "_"
msgid ""
"external ref='figures/gnotski_win.png' md5='5ed32942a09f8becfc784d804f70ea3e'"
msgstr ""
"external ref='figures/gnotski_win.png' md5='5ed32942a09f8becfc784d804f70ea3e'"

#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#.
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/index.docbook:261(imagedata)
msgctxt "_"
msgid ""
"external ref='figures/gnotski_start_window.png' "
"md5='23e584de3f58e5baf5c9ca5098a7961e'"
msgstr ""
"external ref='figures/gnotski_start_window.png' "
"md5='23e584de3f58e5baf5c9ca5098a7961e'"

#: C/index.docbook:28(articleinfo/title)
msgid "<application>Klotski</application> Manual"
msgstr "Εγχειρίδιο του <application>Κλότσκι</application>"

#: C/index.docbook:31(abstract/para)
msgid ""
"Klotski is a puzzle game of which the objective is to get the patterned "
"block to the marker, which is done by moving the blocks in its way."
msgstr ""
"Το Κλότσκι είναι ένα παιχνίδι παζλ του οποίου ο στόχος είναι να μετακινήσετε "
"το μπλοκ με μοτίβο στον δείκτη, και επιτυγχάνεται με την μετακίνηση των "
"μπλοκ που είναι στον δρόμο του."

#: C/index.docbook:38(articleinfo/copyright)
msgid "<year>2002</year> <holder>Andrew Sobala</holder>"
msgstr "<year>2002</year> <holder>Andrew Sobala</holder>"

#: C/index.docbook:54(publisher/publishername)
#: C/index.docbook:142(revdescription/para)
msgid "GNOME Documentation Project"
msgstr "Έργο τεκμηρίωσης GNOME"

#: C/index.docbook:2(legalnotice/para)
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the GNU Free Documentation License (GFDL), Version 1.1 or any "
"later version published by the Free Software Foundation with no Invariant "
"Sections, no Front-Cover Texts, and no Back-Cover Texts. You can find a copy "
"of the GFDL at this <ulink type=\"help\" url=\"help:fdl\">link</ulink> or in "
"the file COPYING-DOCS distributed with this manual."
msgstr ""
"Χορηγείται άδεια αντιγραφής, διανομής και/ή τροποποίησης του παρόντος "
"εγγράφου υπό τους όρους της έκδοσης 1.1 της Ελεύθερης Άδειας Τεκμηρίωσης GNU "
"(GFDL), ή οποιασδήποτε μεταγενέστερης έκδοσής αυτής από το Ίδρυμα Ελεύθερου "
"Λογισμικού (FSF), χωρίς αμετάβλητα τμήματα, κείμενα εξωφύλλου και κείμενα "
"οπισθοφύλλου. Αντίγραφο της άδειας GFDL είναι διαθέσιμο στον ακόλουθο <ulink "
"type=\"help\" url=\"help:fdl\">σύνδεσμο</ulink>, ή στο αρχείο COPYING-DOCS "
"που διανέμεται μαζί με το παρόν εγχειρίδιο."

#: C/index.docbook:12(legalnotice/para) C/legal.xml:12(legalnotice/para)
msgid ""
"This manual is part of a collection of GNOME manuals distributed under the "
"GFDL. If you want to distribute this manual separately from the collection, "
"you can do so by adding a copy of the license to the manual, as described in "
"section 6 of the license."
msgstr ""
"Αυτό το εγχειρίδιο αποτελεί μέρος της συλλογής εγχειριδίων του GNOME που "
"διανέμονται υπό τους όρους της GFDL. Αν επιθυμείτε να διανείμετε το παρόν "
"εγχειρίδιο ξεχωριστά από τη συλλογή, οφείλετε να προσθέσετε στο εγχειρίδιο "
"αντίγραφο της άδειας χρήσης, όπως προβλέπεται στο άρθρο 6 της άδειας."

#: C/index.docbook:19(legalnotice/para) C/legal.xml:19(legalnotice/para)
msgid ""
"Many of the names used by companies to distinguish their products and "
"services are claimed as trademarks. Where those names appear in any GNOME "
"documentation, and the members of the GNOME Documentation Project are made "
"aware of those trademarks, then the names are in capital letters or initial "
"capital letters."
msgstr ""
"Πολλές από τις ονομασίες που χρησιμοποιούνται από εταιρείες για την "
"διαφοροποίηση των προϊόντων και υπηρεσιών τους έχουν καταχωρισθεί ως "
"εμπορικά σήματα. Σε όποιο σημείο της τεκμηρίωσης GNOME τυχόν εμφανίζονται "
"αυτές οι ονομασίες, και εφόσον τα μέλη του Έργου τεκμηρίωσης GNOME έχουν "
"λάβει γνώση αυτών των εμπορικών σημάτων, οι ονομασίες ή τα αρχικά αυτών θα "
"γράφονται με κεφαλαίους χαρακτήρες."

#: C/index.docbook:35(listitem/para) C/legal.xml:35(listitem/para)
msgid ""
"DOCUMENT IS PROVIDED ON AN \"AS IS\" BASIS, WITHOUT WARRANTY OF ANY KIND, "
"EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES THAT "
"THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS FREE OF DEFECTS "
"MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE "
"RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE OF THE DOCUMENT OR "
"MODIFIED VERSION OF THE DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR "
"MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT THE INITIAL "
"WRITER, AUTHOR OR ANY CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY "
"SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN "
"ESSENTIAL PART OF THIS LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED VERSION "
"OF THE DOCUMENT IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER; AND"
msgstr ""
"ΤΟ ΠΑΡΟΝ ΕΓΓΡΑΦΟ ΠΑΡΕΧΕΤΑΙ \"ΩΣ ΕΧΕΙ\", ΧΩΡΙΣ ΟΠΟΙΑΔΗΠΟΤΕ ΑΛΛΗ ΕΓΓΥΗΣΗ, ΕΙΤΕ "
"ΡΗΤΗ ΕΙΤΕ ΣΙΩΠΗΡΗ, ΣΥΜΠΕΡΙΛΑΜΒΑΝΟΜΕΝΗΣ, ΧΩΡΙΣ ΠΕΡΙΟΡΙΣΜΟ, ΤΗΣ ΕΓΓΥΗΣΗΣ ΟΤΙ "
"ΤΟ ΕΓΓΡΑΦΟ, Ή Η ΤΡΟΠΟΠΟΙΗΜΕΝΗ ΕΚΔΟΣΗ ΑΥΤΟΥ, ΕΙΝΑΙ ΕΜΠΟΡΕΥΣΙΜΟ, ΚΑΤΑΛΛΗΛΟ ΓΙΑ "
"ΕΙΔΙΚΟ ΣΚΟΠΟ ΚΑΙ ΔΕΝ ΠΡΟΣΒΑΛΛΕΙ ΔΙΚΑΙΩΜΑΤΑ ΤΡΙΤΩΝ. Ο ΧΡΗΣΤΗΣ ΑΝΑΛΑΜΒΑΝΕΙ ΕΞ "
"ΟΛΟΚΛΗΡΟΥ ΤΗΝ ΕΘΥΝΗ ΩΣ ΠΡΟΣ ΤΗΝ ΠΟΙΟΤΗΤΑ, ΤΗΝ ΑΚΡΙΒΕΙΑ ΚΑΙ ΤΗΝ ΧΡΗΣΗ ΤΟΥ "
"ΕΓΓΡΑΦΟΥ Ή ΤΗΣ ΤΡΟΠΟΠΟΙΗΜΕΝΗΣ ΕΚΔΟΣΗΣ ΑΥΤΟΥ. ΣΕ ΠΕΡΙΠΤΩΣΗ ΠΟΥ ΟΠΟΙΟΔΗΠΟΤΕ "
"ΕΓΓΡΑΦΟ Ή ΤΡΟΠΟΠΟΙΗΜΕΝΗ ΕΚΔΟΣΗ ΑΥΤΟΥ ΑΠΟΔΕΙΧΘΟΥΝ ΕΛΑΤΤΩΜΑΤΙΚΑ ΚΑΘ' "
"ΟΙΟΝΔΗΠΟΤΕ ΤΡΟΠΟ, Ο ΧΡΗΣΤΗΣ (ΚΑΙ ΟΧΙ Ο ΑΡΧΙΚΟΣ ΣΥΓΓΡΑΦΕΑΣ, ΔΗΜΙΟΥΡΓΟΣ Ή "
"ΟΠΟΙΟΣΔΗΠΟΤΕ ΣΥΝΤΕΛΕΣΤΗΣ) ΑΝΑΛΑΜΒΑΝΕΙ ΤΟ ΚΟΣΤΟΣ ΟΠΟΙΑΣΔΗΠΟΤΕ ΑΝΑΓΚΑΙΑΣ "
"ΣΥΝΤΗΡΗΣΗΣ, ΕΠΙΣΚΕΥΗΣ Ή ΔΙΟΡΘΩΣΗΣ. Η ΠΑΡΟΥΣΑ ΑΠΟΠΟΙΗΣΗ ΕΓΓΥΗΣΗΣ ΑΠΟΤΕΛΕΙ "
"ΑΝΑΠΟΣΠΑΣΤΟ ΜΕΡΟΣ ΤΗΣ ΑΔΕΙΑΣ. ΔΕΝ ΕΠΙΤΡΕΠΕΤΑΙ ΟΥΔΕΜΙΑ ΧΡΗΣΗ ΤΟΥ ΕΓΓΡΑΦΟΥ Ή "
"ΤΡΟΠΟΠΟΙΗΜΕΝΩΝ ΕΚΔΟΣΕΩΝ ΑΥΤΟΥ ΣΥΜΦΩΝΑ ΜΕ ΤΟΥΣ ΟΡΟΥΣ ΤΗΣ ΠΑΡΟΥΣΑΣ,  ΠΑΡΑ ΜΟΝΟ "
"ΕΑΝ ΣΥΝΟΔΕΥΕΤΑΙ ΑΠΟ ΤΗΝ ΑΠΟΠΟΙΗΣΗ ΕΓΓΥΗΣΗΣ,  ΚΑΙ"

#: C/index.docbook:55(listitem/para) C/legal.xml:55(listitem/para)
msgid ""
"UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER IN TORT (INCLUDING "
"NEGLIGENCE), CONTRACT, OR OTHERWISE, SHALL THE AUTHOR, INITIAL WRITER, ANY "
"CONTRIBUTOR, OR ANY DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION OF THE "
"DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH PARTIES, BE LIABLE TO ANY PERSON "
"FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF "
"ANY CHARACTER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, "
"WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER DAMAGES "
"OR LOSSES ARISING OUT OF OR RELATING TO USE OF THE DOCUMENT AND MODIFIED "
"VERSIONS OF THE DOCUMENT, EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF THE "
"POSSIBILITY OF SUCH DAMAGES."
msgstr ""
"Ο ΔΗΜΙΟΥΡΓΟΣ, Ο ΑΡΧΙΚΟΣ ΣΥΓΓΡΑΦΕΑΣ, ΟΙ ΣΥΝΤΕΛΕΣΤΕΣ Ή ΟΙ ΔΙΑΝΟΜΕΙΣ ΤΟΥ "
"ΕΓΓΡΑΦΟΥ Ή ΤΡΟΠΟΠΟΙΗΜΕΝΗΣ ΕΚΔΟΣΗΣ ΑΥΤΟΥ, ΚΑΘΩΣ ΚΑΙ ΟΙ ΠΡΟΜΗΘΕΥΤΕΣ "
"ΟΠΟΙΩΝΔΗΠΟΤΕ ΕΚ ΤΩΝ ΠΡΟΑΝΑΦΕΡΟΜΕΝΩΝ ΜΕΡΩΝ, ΔΕΝ ΕΥΘΥΝΟΝΤΑΙ ΕΝΑΝΤΙ ΟΙΟΥΔΗΠΟΤΕ, "
"ΣΕ ΚΑΜΙΑ ΠΕΡΙΠΤΩΣΗ ΚΑΙ ΥΠΟ ΚΑΜΙΑ ΕΡΜΗΝΕΙΑ ΝΟΜΟΥ, ΕΙΤΕ ΕΞ ΑΔΙΚΟΠΡΑΞΙΑΣ "
"(ΣΥΜΠΕΡΙΛΑΜΒΑΝΟΜΕΝΗΣ ΤΗΣ ΑΜΕΛΕΙΑΣ) ΕΙΤΕ ΣΤΟ ΠΛΑΙΣΙΟ ΣΥΜΒΑΤΙΚΗΣ Ή ΑΛΛΗΣ "
"ΥΠΟΧΡΕΩΣΗΣ, ΓΙΑ ΤΥΧΟΝ ΑΜΕΣΕΣ, ΕΜΜΕΣΕΣ, ΕΙΔΙΚΕΣ, ΤΥΧΑΙΕΣ Ή ΣΥΝΕΠΑΚΟΛΟΥΘΕΣ "
"ΖΗΜΙΕΣ ΟΠΟΙΑΣΔΗΠΟΤΕ ΜΟΡΦΗΣ, ΣΥΜΠΕΡΙΛΑΜΒΑΝΟΜΕΝΩΝ, ΧΩΡΙΣ ΠΕΡΙΟΡΙΣΜΟ, ΖΗΜΙΩΝ "
"ΛΟΓΩ ΑΠΩΛΕΙΑΣ ΦΗΜΗΣ ΚΑΙ ΠΕΛΑΤΕΙΑΣ, ΔΙΑΚΟΠΗΣ ΕΡΓΑΣΙΩΝ, ΔΥΣΛΕΙΤΟΥΡΓΙΑΣ Ή "
"ΒΛΑΒΗΣ ΗΛΕΚΤΡΟΝΙΚΩΝ ΥΠΟΛΟΓΙΣΤΩΝ, Ή ΚΑΘΕ ΑΛΛΗΣ ΖΗΜΙΑΣ Ή ΑΠΩΛΕΙΑΣ ΠΟΥ "
"ΟΦΕΙΛΕΤΑΙ Ή ΣΧΕΤΙΖΕΤΑΙ ΜΕ ΤΗΝ ΧΡΗΣΗ ΤΟΥ ΕΓΓΡΑΦΟΥ ΚΑΙ ΤΩΝ ΤΡΟΠΟΠΟΙΗΜΕΝΩΝ "
"ΕΚΔΟΣΕΩΝ ΑΥΤΟΥ, ΑΚΟΜΑ ΚΑΙ ΑΝ ΤΑ ΩΣ ΑΝΩ ΜΕΡΗ ΕΙΧΑΝ ΛΑΒΕΙ ΓΝΩΣΗ ΤΗΣ "
"ΠΙΘΑΝΟΤΗΤΑΣ ΠΡΟΚΛΗΣΗΣ ΤΕΤΟΙΩΝ ΖΗΜΙΩΝ."

#: C/index.docbook:28(legalnotice/para) C/legal.xml:28(legalnotice/para)
msgid ""
"DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED UNDER THE TERMS "
"OF THE GNU FREE DOCUMENTATION LICENSE WITH THE FURTHER UNDERSTANDING THAT: "
"<_:orderedlist-1/>"
msgstr ""
"ΤΟ ΠΑΡΟΝ ΕΓΓΡΑΦΟ ΚΑΙ ΟΙ ΤΡΟΠΟΙΗΜΕΝΕΣ ΕΚΔΟΣΕΙΣ ΑΥΤΟΥ ΠΑΡΕΧΟΝΤΑΙ ΥΠΟ ΤΟΥΣ "
"ΟΡΟΥΣ ΤΗΣ ΕΛΕΥΘΕΡΗΣ ΑΔΕΙΑΣ ΤΕΚΜΗΡΙΩΣΗΣ GNU (GFDL) ΚΑΙ ΜΕ ΤΗΝ ΠΕΡΑΙΤΕΡΩ "
"ΔΙΕΥΚΡΙΝΙΣΗ ΟΤΙ: <_:orderedlist-1/>"

#: C/index.docbook:63(authorgroup/author)
msgid ""
"<firstname>Andrew</firstname> <surname>Sobala</surname> <affiliation> "
"<orgname>GNOME Documentation Project</orgname> <address> "
"<email>andrew@sobala.net</email> </address> </affiliation>"
msgstr ""
"<firstname>Andrew</firstname> <surname>Sobala</surname> <affiliation> "
"<orgname>Έργο τεκμηρίωσης GNOME</orgname> <address> <email>andrew@sobala."
"net</email> </address> </affiliation>"

#: C/index.docbook:72(authorgroup/othercredit)
msgid ""
"<firstname>Lars</firstname> <surname>Rydlinge</surname> <affiliation> "
"<orgname>GNOME Project</orgname> <address> <email> Lars.Rydlinge@HIG.SE </"
"email> </address> </affiliation>"
msgstr ""
"<firstname>Lars</firstname> <surname>Rydlinge</surname> <affiliation> "
"<orgname>Έργο GNOME</orgname> <address> <email> Lars.Rydlinge@HIG.SE </"
"email> </address> </affiliation>"

#: C/index.docbook:81(authorgroup/othercredit)
msgid ""
"<firstname>Ross</firstname> <surname>Burton</surname> <affiliation> "
"<orgname>GNOME Project</orgname> <address> <email> ross@burtonini.com </"
"email> </address> </affiliation>"
msgstr ""
"<firstname>Ross</firstname> <surname>Burton</surname> <affiliation> "
"<orgname>Έργο GNOME</orgname> <address> <email> ross@burtonini.com </email> "
"</address> </affiliation>"

#: C/index.docbook:139(revdescription/para)
msgid "Andrew Sobala <email>andrew@sobala.net</email>"
msgstr "Andrew Sobala <email>andrew@sobala.net</email>"

#: C/index.docbook:135(revhistory/revision)
msgid ""
"<revnumber>V2.0</revnumber> <date>13 August 2002</date> <_:revdescription-1/>"
msgstr ""
"<revnumber>V2.0</revnumber> <date>13 Αυγούστου 2002</date> <_:"
"revdescription-1/>"

#: C/index.docbook:148(articleinfo/releaseinfo)
msgid "This manual describes version 2.12 of GNOME Klotski."
msgstr "Αυτό το εγχειρίδιο περιγράφει την έκδοση 2.12 του Κλότσκι."

#: C/index.docbook:151(legalnotice/title)
msgid "Feedback"
msgstr "Aνάδραση"

#: C/index.docbook:152(legalnotice/para)
msgid ""
"To report a bug or make a suggestion regarding the <application>Klotski</"
"application> application or this manual, follow the directions in the <ulink "
"url=\"ghelp:user-guide?feedback-bugs\" type=\"help\">GNOME Feedback Page</"
"ulink>."
msgstr ""
"Για να αναφέρετε ένα σφάλμα ή να κάνετε μια πρόταση σχετικά με την εφαρμογή "
"<application>Κλότσκι</application>, ή αυτό το εγχειρίδιο, ακολουθήστε τις "
"οδηγίες στη <ulink url=\"ghelp:user-guide?feedback-bugs\" type=\"help"
"\">Σελίδα ανάδρασης GNOME</ulink>."

#: C/index.docbook:160(article/indexterm)
msgid "<primary>GNOME Klotski</primary>"
msgstr "<primary>Κλότσκι</primary>"

#: C/index.docbook:167(sect1/title)
msgid "Introduction"
msgstr "Εισαγωγή"

#: C/index.docbook:168(sect1/para)
msgid ""
"The <application>Klotski</application> application is a clone of the Klotski "
"game. The objective is to move the patterned block to the area bordered by "
"green markers."
msgstr ""
"Η εφαρμογή <application>Κλότσκι</application> είναι ένας κλώνος του "
"παιχνιδιού Κλότσκι. Ο στόχος είναι να μετακινήσετε το μπλοκ με μοτίβο στην "
"περιοχή που οριοθετείται από τους πράσινους δείκτες."

#: C/index.docbook:171(sect1/para)
msgid ""
"<application>Klotski</application> was written by Lars Rydlinge (<email>Lars."
"Rydlinge@HIG.SE</email>)."
msgstr ""
"Το <application>Κλότσκι</application> γράφτηκε από τον Lars Rydlinge "
"(<email>Lars.Rydlinge@HIG.SE</email>)."

#: C/index.docbook:174(sect1/para)
msgid ""
"<application>Klotski</application> is included in the gnome-games package, "
"which is part of the GNOME desktop environment. This document describes "
"version 2.12 of <application>Klotski</application>."
msgstr ""
"To <application>Κλότσκι</application> συμπεριλαμβάνεται στο πακέτο gnome-"
"games, το οποίο είναι κομμάτι του περιβάλλοντος επιφάνειας εργασίας GNOME. "
"Αυτό το έγγραφο περιγράφει την έκδοση 2.12 του <application>Κλότσκι</"
"application>."

#: C/index.docbook:177(sect1/para)
msgid ""
"This program is distributed under the terms of the GNU General Public "
"license as published by the Free Software Foundation; either version 2 of "
"the License, or (at your option) any later version. A copy of this license "
"can be found at this <ulink url=\"help:gpl\" type=\"help\">link</ulink>, or "
"in the file COPYING included with the source code of this program."
msgstr ""
"Αυτό το πρόγραμμα διανέμεται υπό τους όρους της  Γενικής Άδειας Δημόσιας "
"Χρήσης GNU (GPL), όπως αυτή έχει δημοσιευτεί από το Ίδρυμα Ελεύθερου "
"Λογισμικού (FSS) — είτε της έκδοσης 2 της ‘Αδειας, είτε (κατ' επιλογήν σας) "
"οποιασδήποτε μεταγενέστερης έκδοσης. Αντίγραφο της άδειας είναι διαθέσιμο "
"στον ακόλουθο  <ulink url=\"help:gpl\" type=\"help\">σύνδεσμο</ulink>, ή στο "
"αρχείο COPYING-DOCS που διανέμεται μαζί με τον πηγαίο κώδικα αυτού του "
"προγράμματος."

#: C/index.docbook:184(sect1/para)
msgid ""
"To report a bug or make a suggestion regarding this application or this "
"manual, follow the directions in this <ulink url=\"ghelp:user-guide?feedback-"
"bugs\" type=\"help\">document</ulink>."
msgstr ""
"Για να αναφέρετε ένα σφάλμα ή να κάνετε μια πρόταση σχετικά με αυτήν την "
"εφαρμογή ή αυτό το εγχειρίδιο, ακολουθήστε τις οδηγίες σε αυτό το <ulink url="
"\"ghelp:user-guide?feedback-bugs\" type=\"help\">έγγραφο</ulink>."

#: C/index.docbook:193(sect1/title)
msgid "Objective"
msgstr "Σκοπός"

#: C/index.docbook:200(figure/title)
msgid "The winning move"
msgstr "Η κίνηση νίκης"

#: C/index.docbook:202(screenshot/mediaobject)
msgid ""
"<imageobject> <imagedata fileref=\"figures/gnotski_win.png\" format=\"PNG\"/"
"> </imageobject> <textobject> <phrase>The patterned block should be moved to "
"the space bordered by the green markers.</phrase> </textobject>"
msgstr ""
"<imageobject> <imagedata fileref=\"figures/gnotski_win.png\" format=\"PNG\"/"
"> </imageobject> <textobject> <phrase>Το μπλοκ με μοτίβο πρέπει να "
"μετακινηθεί στο χώρο που οριοθετείται από τους πράσινους δείκτες</phrase> </"
"textobject>"

#: C/index.docbook:194(sect1/para)
msgid ""
"The objective of <application>Klotski</application> is to move the patterned "
"block to the green markers in as few moves as possible. You do this by "
"moving the blocks one at a time with the mouse until you can get the "
"patterned block to the markers. <_:figure-1/>"
msgstr ""
"Ο στόχος του <application>Κλότσκι</application> είναι να μετακινηθεί το "
"μπλοκ με μοτίβο στους πράσινους δείκτες σε όσο λιγότερες κινήσεις γίνεται. "
"Μπορείτε να το κάνετε αυτό μετακινώντας τα μπλοκ ένα - ένα με το ποντίκι "
"μέχρι να πάει το μπλοκ με το μοτίβο στους δείκτες. <_:figure-1/>"

#: C/index.docbook:219(sect1/title)
msgid "Playing <application>Klotski</application>"
msgstr "Παίζοντας <application>Κλότσκι</application>"

#: C/index.docbook:242(sect2/title)
msgid "To Start <application>Klotski</application>"
msgstr "Εκκίνηση του <application>Κλότσκι</application>"

#: C/index.docbook:243(sect2/para)
msgid ""
"You can start <application>Klotski</application> from the Applications menu "
"by choosing <menuchoice> <guisubmenu>Games</guisubmenu> "
"<guimenuitem>Gnotski</guimenuitem> </menuchoice>."
msgstr ""
"Μπορείτε να εκκινήσετε το <application>Κλότσκι</application> από το μενού "
"Εφαρμογές επιλέγοντας <menuchoice> <guisubmenu>Παιχνίδια</guisubmenu> "
"<guimenuitem>Κλότσκι</guimenuitem> </menuchoice>."

#: C/index.docbook:252(sect2/title)
msgid "The main window"
msgstr "Το αρχικό παράθυρο"

#: C/index.docbook:253(sect2/para)
msgid ""
"When you start <application><application>Klotski</application></"
"application>, the following window is displayed."
msgstr ""
"Όταν εκκινείτε το <application>Κλότσκι</application>, το ακόλουθο παράθυρο "
"εμφανίζεται."

#: C/index.docbook:257(figure/title)
msgid "<application>Klotski</application> Start Up Window"
msgstr "Το παράθυρο εκκίνησης <application>Κλότσκι</application>"

#: C/index.docbook:259(screenshot/mediaobject)
msgid ""
"<imageobject> <imagedata fileref=\"figures/gnotski_start_window.png\" format="
"\"PNG\"/> </imageobject> <textobject> <phrase>Shows <application>Klotski</"
"application> main window. Contains titlebar, menubar, game area and "
"statusbar. Menubar contains Game and Help menus. </phrase> </textobject>"
msgstr ""
"<imageobject> <imagedata fileref=\"figures/gnotski_start_window.png\" format="
"\"PNG\"/> </imageobject> <textobject> <phrase>Εμφανίζει το κύριο παράθυρο "
"του <application>Κλότσκι</application>. Περιέχει τη γραμμή τίτλου,του μενού, "
"τη περιοχή του παιχνιδιού και τη γραμμή κατάστασης. Η γραμμή του μενού "
"περιέχει τα μενού Παιχνίδι και Βοήθεια. </phrase> </textobject>"

#: C/index.docbook:275(varlistentry/term)
msgid "Menubar"
msgstr "Γραμμή μενού"

#: C/index.docbook:279(varlistentry/term)
msgid "Game menu"
msgstr "Μενού παιχνιδιού"

#: C/index.docbook:281(listitem/para)
msgid ""
"The game menu allows you to start new games, view the highest scores and "
"quit the program."
msgstr ""
"Το μενού Παιχνίδι σας επιτρέπει να ξεκινήσετε νέα παιχνίδια, να δείτε τις "
"υψηλότερες βαθμολογίες και να βγείτε από το πρόγραμμα."

#: C/index.docbook:286(varlistentry/term)
msgid "Help menu"
msgstr "Μενού βοήθειας"

#: C/index.docbook:288(listitem/para)
msgid ""
"The help menu allows you to view the documentation for <application>Klotski</"
"application>"
msgstr ""
"Το μενού Βοήθεια σας επιτρέπει να προβάλετε την τεκμηρίωση του "
"<application>Κλότσκι</application>"

#: C/index.docbook:295(varlistentry/term)
msgid "Game area"
msgstr "Περιοχή παιχνιδιού"

#: C/index.docbook:297(listitem/para)
msgid ""
"The game area shows the blocks in the current game. You can drag the blocks "
"around with the mouse as described in <xref linkend=\"gnotski-moving\"/>."
msgstr ""
"Η περιοχή παιχνιδιού εμφανίζει τα μπλοκ στο τρέχον παιχνίδι. Μπορείτε να "
"σύρετε τα μπλοκ με το ποντίκι όπως περιγράφεται στο <xref linkend=\"gnotski-"
"moving\"/>."

#: C/index.docbook:303(varlistentry/term)
msgid "Statusbar"
msgstr "Γραμμή κατάστασης"

#: C/index.docbook:305(listitem/para)
msgid "The statusbar shows how many moves you have taken so far."
msgstr "Η γραμμή κατάστασης εμφανίζει πόσες κινήσεις έχετε κάνει μέχρι τώρα."

#: C/index.docbook:272(sect2/para)
msgid ""
"The <application>Klotski</application> window contains the following "
"elements: <_:variablelist-1/>"
msgstr ""
"Το παράθυρο του <application>Κλότσκι</application> περιέχει τα ακόλουθα "
"στοιχεία: <_:variablelist-1/>"

#: C/index.docbook:314(sect2/title)
msgid "Starting a new game"
msgstr "Ξεκινώντας ένα νέο παιχνίδι"

#: C/index.docbook:315(sect2/para)
msgid ""
"Use the <guimenu>Game menu</guimenu> to start a new game. The items "
"<guimenuitem>Next Puzzle</guimenuitem> and <guimenuitem>Previous Puzzle</"
"guimenuitem> allows you to navigate the puzzles sequentially. Each puzzle "
"belongs to one of the three categories found in the submenus named "
"<guisubmenu>HuaRong Trail</guisubmenu>, <guisubmenu>Challenge Pack</"
"guisubmenu> and <guisubmenu>Skill Pack</guisubmenu>.In these submenus, each "
"puzzle name refers to a game scenario."
msgstr ""
"Χρησιμοποιήστε το <guimenu>Μενού παιχνιδιού</guimenu> για να ξεκινήσετε ένα "
"νέο παιχνίδι. Οι επιλογές <guimenuitem>Επόμενο παζλ</guimenuitem> και "
"<guimenuitem>Προηγούμενο παζλ</guimenuitem> σας επιτρέπουν να περιηγηθείτε "
"στα παζλ διαδοχικά. Κάθε παζλ ανήκει σε μια από τις τρεις κατηγορίες που "
"βρίσκονται στα μενού με όνομα <guisubmenu>Διαδρομή HuaRong</guisubmenu>, "
"<guisubmenu>Πακέτο προκλήσεων</guisubmenu> και  <guisubmenu>Πακέτο "
"δεξιοτήτων</guisubmenu>. Σε αυτά τα μενού, κάθε όνομα παζλ αναφέρεται σε ένα "
"σενάριο παιχνιδιού."

#: C/index.docbook:323(example/title)
msgid "Starting a game"
msgstr "Ξεκινώντας ένα παιχνίδι"

#: C/index.docbook:324(example/para)
msgid ""
"To start a game, choose <menuchoice> <guimenu>Game</guimenu> <guisubmenu>"
"[category]</guisubmenu> <guimenuitem>[game scenario]</guimenuitem> </"
"menuchoice>"
msgstr ""
"Για να ξεκινήσετε ένα παιχνίδι, επιλέξτε <menuchoice> <guimenu>Παιχνίδι</"
"guimenu> <guisubmenu>[κατηγορία]</guisubmenu> <guimenuitem>[σενάριο "
"παιχνιδιού]</guimenuitem> </menuchoice>"

#: C/index.docbook:335(sect2/title)
msgid "Moving blocks"
msgstr "Μετακινώντας τα μπλοκ"

#: C/index.docbook:336(sect2/para)
msgid ""
"To move a block, click on it, hold down the mouse button and drag it to its "
"new location. You cannot move the green blocks or the block that borders the "
"game. You cannot move a block to a space already occupied by another block, "
"with the exception that you may move the patterned block through the green "
"blocks to win."
msgstr ""
"Για να μετακινήσετε ένα μπλοκ, κάντε κλικ πάνω του, κρατήστε το πλήκτρο του "
"ποντικιού και σύρετε το στην νέα του τοποθεσία. Δεν μπορείτε να μετακινήσετε "
"τα πράσινα μπλοκ ή το μπλοκ που οριοθετεί το παιχνίδι. Δεν μπορείτε να "
"μετακινήσετε ένα μπλοκ σε σημείο που υπάρχει ήδη άλλο μπλοκ, με την εξαίρεση "
"πως μπορείτε να μετακινήσετε το μπλοκ με μοτίβο μέσα από τα πράσινα μπλοκ "
"για να νικήσετε."

#: C/index.docbook:356(sect1/title)
msgid "Authors"
msgstr "Δημιουργοί"

#: C/index.docbook:357(sect1/para)
msgid ""
"<application>Klotski</application> was written by Lars Rydlinge (<email>lars."
"rydlinge@hig.se</email>). This manual was written by Andrew Sobala. To "
"report a bug or make a suggestion regarding this application or this manual, "
"follow the directions in this <ulink url=\"ghelp:user-guide?feedback-bugs\" "
"type=\"help\">document</ulink>."
msgstr ""
"Το <application>Κλότσκι</application> γράφτηκε από τον Lars Rydlinge "
"(<email>lars.rydlinge@hig.se</email>). Αυτό το εγχειρίδιο γράφτηκε από τον "
"Andrew Sobala. Για να αναφέρετε κάποιο πρόβλημα ή να κάνετε μια πρόταση "
"σχετικά με αυτή την εφαρμογή ή αυτό το εγχειρίδιο, ακολουθήστε τις οδηγίες "
"σε αυτό το <ulink url=\"ghelp:user-guide?feedback-bugs\" type=\"help"
"\">έγγραφο</ulink>."

#: C/index.docbook:377(sect1/title)
msgid "License"
msgstr "Άδεια χρήσης"

#: C/index.docbook:378(sect1/para)
msgid ""
"This program and tile artwork is free software; you can redistribute it and/"
"or modify it under the terms of the <citetitle>GNU General Public License</"
"citetitle> as published by the Free Software Foundation; either version 2 of "
"the License, or (at your option) any later version."
msgstr ""
"Αυτό το πρόγραμμα είναι ελεύθερο λογισμικό· επιτρέπεται η αναδιανομή ή/και "
"τροποποίησή του υπό τους όρους της  <citetitle>Γενικής Άδειας Δημόσιας "
"Χρήσης GNU (GPL)</citetitle>, όπως αυτή έχει δημοσιευτεί από το Ίδρυμα "
"Ελεύθερου Λογισμικού (FSS) — είτε της έκδοσης 2 της Άδειας, είτε (κατ' "
"επιλογήν σας) οποιασδήποτε μεταγενέστερης έκδοσης. "

#: C/index.docbook:385(sect1/para)
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE. See the <citetitle>GNU General Public "
"License</citetitle> for more details."
msgstr ""
"Το πρόγραμμα αυτό διανέμεται με την ελπίδα ότι θα αποδειχθεί χρήσιμο, παρόλα "
"αυτά ΧΩΡΙΣ ΚΑΜΙΑ ΕΓΓΥΗΣΗ — χωρίς ούτε και την σιωπηρή εγγύηση "
"ΕΜΠΟΡΕΥΣΙΜΟΤΗΤΑΣ ή ΚΑΤΑΛΛΗΛΟΤΗΤΑΣ ΓΙΑ ΕΙΔΙΚΟ ΣΚΟΠΟ. Για περισσότερες "
"λεπτομέρειες ανατρέξτε στη <citetitle>Γενική Άδεια Δημόσιας Χρήσης GNU (GPL)"
"</citetitle>."

#: C/index.docbook:398(para/address)
#, no-wrap
msgid ""
"\n"
"    Free Software Foundation, Inc.\n"
"    <street>59 Temple Place</street> - Suite 330\n"
"    <city>Boston</city>, <state>MA</state> <postcode>02111-1307</postcode>\n"
"    <country>USA</country>\n"
"   "
msgstr ""
"\n"
"    Ίδρυμα Ελεύθερου Λογισμικού, Inc.\n"
"    <street>59 Temple Place</street> - Suite 330\n"
"    <city>Βοστόνη</city>, <state>MA</state> <postcode>02111-1307</postcode>\n"
"    <country>Η.Π.Α</country>\n"
"   "

#: C/index.docbook:391(sect1/para)
msgid ""
"A copy of the <citetitle>GNU General Public License</citetitle> is included "
"as an appendix to the <citetitle>GNOME Users Guide</citetitle>. You may also "
"obtain a copy of the <citetitle>GNU General Public License</citetitle> from "
"the Free Software Foundation by visiting <ulink type=\"http\" url=\"http://"
"www.fsf.org\">their Web site</ulink> or by writing to <_:address-1/>"
msgstr ""
"Αντίγραφο της <citetitle>Γενικής Άδειας Δημόσιας Χρήσης GNU (GPL)</"
"citetitle> περιλαμβάνεται ως παράρτημα στον <citetitle>Οδηγό χρήστη GNOME</"
"citetitle>. Επίσης, αντίγραφο της <citetitle>Γενικής Άδειας Δημόσιας Χρήσης "
"GNU (GPL)</citetitle> μπορείτε να παραλάβετε από το  Ίδρυμα Ελεύθερου "
"Λογισμικού (FSS), είτε επισκεπτόμενοι τον <ulink type=\"http\" url=\"http://"
"www.fsf.org\">ιστοχώρο</ulink> του, είτε γράφοντας στη διεύθυνση <_:"
"address-1/>"

#: C/legal.xml:9(para/ulink)
msgid "link"
msgstr "σύνδεσμο"

#: C/legal.xml:2(legalnotice/para)
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the GNU Free Documentation License (GFDL), Version 1.1 or any "
"later version published by the Free Software Foundation with no Invariant "
"Sections, no Front-Cover Texts, and no Back-Cover Texts. You can find a copy "
"of the GFDL at this <_:ulink-1/> or in the file COPYING-DOCS distributed "
"with this manual."
msgstr ""
"Χορηγείται άδεια αντιγραφής, διανομής και/ή τροποποίησης του παρόντος "
"εγγράφου υπό τους όρους της έκδοσης 1.1 της Ελεύθερης Άδειας Τεκμηρίωσης GNU "
"(GFDL), ή οποιασδήποτε μεταγενέστερης έκδοσής αυτής από το Ίδρυμα Ελεύθερου "
"Λογισμικού (FSF), χωρίς αμετάβλητα τμήματα, κείμενα εξωφύλλου και κείμενα "
"οπισθόφυλλου. Αντίγραφο της άδειας GFDL είναι διαθέσιμο στον ακόλουθο <_:"
"ulink-1/>, ή στο αρχείο COPYING-DOCS που διανέμεται μαζί με το παρόν "
"εγχειρίδιο."

#~ msgid "2002"
#~ msgstr "2002"

#~ msgid "Andrew Sobala"
#~ msgstr "Andrew Sobala"

#~ msgid "Andrew"
#~ msgstr "Andrew"

#~ msgid "Sobala"
#~ msgstr "Sobala"

#~ msgid "andrew@sobala.net"
#~ msgstr "andrew@sobala.net"

#~ msgid "Lars"
#~ msgstr "Lars"

#~ msgid "Rydlinge"
#~ msgstr "Rydlinge"

#~ msgid "GNOME Project"
#~ msgstr "Έργο GNOME"

#~ msgid "Lars.Rydlinge@HIG.SE"
#~ msgstr "Lars.Rydlinge@HIG.SE"

#~ msgid "Ross"
#~ msgstr "Ross"

#~ msgid "Burton"
#~ msgstr "Burton"

#~ msgid "ross@burtonini.com"
#~ msgstr "ross@burtonini.com"

#~ msgid "V2.0"
#~ msgstr "V2.0"

#~ msgid "13 August 2002"
#~ msgstr "13 Αυγούστου 2002"

#~ msgid "GNOME Klotski"
#~ msgstr "GNOME Κλότσκι"

#~ msgid ""
#~ "The patterned block should be moved to the space bordered by the green "
#~ "markers."
#~ msgstr ""
#~ "Το μπλοκ με μοτίβο πρέπει να μετακινηθεί στο χώρο που οριοθετείται από "
#~ "τους πράσινους δείκτες."

#~ msgid "Klotski"
#~ msgstr "Κλότσκι"

#~ msgid ""
#~ "Shows <placeholder-1/> main window. Contains titlebar, menubar, game area "
#~ "and statusbar. Menubar contains Game and Help menus."
#~ msgstr ""
#~ "Εμφανίζει το <placeholder-1/> κεντρικό παράθυρο. Περιέχει γραμμή τίτλου, "
#~ "γραμμή μενού, περιοχή παιχνιδιού και γραμμή κατάστασης. Η γραμμή μενού "
#~ "περιέχει τα μενού Παιχνίδι και Βοήθεια"
